package ch.ffhs.dua.pva.list;

import java.util.Iterator;
import java.util.List;

public class LinkedList<E> extends ListBasic<E> implements List<E> {

	private Node<E> anchor;
	private Node<E> sail;
	private int size;

	public LinkedList() {
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public E get(int index) {
		int i = 0;
		Node<E> tmp = anchor;
		for (i = 0; i <= index; i++) {
			if (i == index) {
				return (E) tmp.element;
			}
			if (tmp.next == null) {
				System.out.println("Out of bounds");
				return null;
			}
			tmp = tmp.next;
		}
		return null;
	}

	@Override
	public E set(int index, E element) {
		int i = 0;
		Node<E> tmp = anchor;
		while (tmp.next != null) {

			if (i == index) {
				tmp.element = element;
				return element;
			}
			tmp = tmp.next;
			i++;
		}
		return element;
	}

	@Override
	public boolean add(E element) {
		System.out.println("Adding node with value " + element);

		Node<E> new_node = new Node<E>(element, null, sail);
		if (sail != null) {
			sail.next = new_node;
		}
		sail = new_node;

		if (anchor == null) {
			anchor = new_node;
		}

		size++;
		return true;
	}

	@Override
	public void add(int index, E element) {
		Node<E> tmp = anchor;
		Node<E> new_element = new Node<E>(element, null, null);
		

		if(index == 0) {
			// Add to beginning
			new_element.next = tmp;
			tmp.prev = new_element;
			anchor = new_element;
		} else if(index == (size)){
			// add to end
			sail.next = new_element;
			new_element.prev = sail;
			sail = new_element;
		} else {
			// add element in the middle
			for (int i=0;i<=index;i++) {
				if (index == i) {
					new_element.next = tmp;
					new_element.prev = tmp.prev;

					tmp.prev.next = new_element;
					tmp.prev = new_element;
					break;
				}
				tmp = tmp.next;
			}
		}
		size++;
	}

	public boolean contains(Object o) {
		Node<E> tmp = anchor;
		for (int i=0;i<size;i++) {

			if (tmp.element == o) {
				return true;
			}
			tmp = tmp.next;
		}
		return false;
	}

	@Override
	public E remove(int index) {
		if (size == 0) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> tmp = anchor;
		E current = null;
		int i = 0;
		while (tmp.next != null) {
			if (i == index) {
				current = ((E) tmp.element);
				if (tmp.prev == null) {
					anchor = tmp.next;

					if (!isEmpty()) {
						anchor.prev = null;
					}
				} else if (tmp.next == null) {
					sail = tmp.prev;

					if (!isEmpty()) {
						anchor.prev = null;
					}
				} else {
					tmp.next.prev = tmp.prev;
					tmp.prev.next = tmp.next;

				}
			}
			i++;
			tmp = tmp.next;
		}
		size--;
		return current;
	}

	@Override
	public boolean remove(Object o) {
	    // Method not used --> will not implement
		return false;
	}

	@Override
	public void clear() {
        // Method not used --> will not implement
        anchor = null;
        sail = null;
        size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new LinkedListIterator();
	}

	///////////////////////////////////////////////////

	/**
	 * This class defines a Node for the LinkedList
	 * 
	 * @param <E>
	 */
	private static class Node<E> {
		E element;
		String text;
		Node<E> next;
		Node<E> prev;

		/**
		 * Constructor class for the LinkedList
		 * 
		 * @param element
		 * @param next
		 * @param prev
		 */
		public Node(E element, Node<E> next, Node<E> prev) {
			this.element = element;
			this.text = (String) element;
			this.next = next;
			this.prev = prev;
		}

		@Override
		public String toString() {

			// TODO Auto-generated method stub
			return text;
		}
	}

	private class LinkedListIterator implements Iterator<E> {

		private Node<E> current;

		@Override
		public boolean hasNext() {
			if (current == null) {
				return anchor.next != null;
			}
			return current.next != null;
		}

		@Override
		public E next() {
			if (!hasNext()) {
				return null;
			}
			if (current == null) {
				current = anchor;
			} else {
				current = current.next;
			}

			return (E) current.element;
		}

		@Override
		public void remove() {

			if (current.equals(anchor)) {
				current = current.next;
				current.prev = null;
			} else if (!hasNext()) {
				current = current.prev;
				current.next = null;

			} else {
				current.next.prev = current.prev;
				current.prev.next = current.next;
			}
			--size;
		}
	}

	public static void main(String a[]) {

		LinkedList<String> ll = new LinkedList<>();

		System.out.println("The size is; " + ll.size());
		ll.add("A");
		System.out.println("The size is; " + ll.size());
		ll.add("B");
		System.out.println("The size is; " + ll.size());
		ll.add("D");
		ll.add("E");
		ll.add("F");
		System.out.println("List contains B: " + ll.contains("B"));
		System.out.println("Getting element 1: " + ll.get(1));
		ll.set(1, "X");
		System.out.println("List contains B: " + ll.contains("B"));
		System.out.println("The size is; " + ll.size());
		System.out.println("Getting element 1: " + ll.get(1));
		ll.remove(1);
		ll.add(0, "G");
		ll.add(2, "Q");
		System.out.println("Getting element 2: " + ll.get(2));
		System.out.println("The size is; " + ll.size());
		System.out.println(ll.anchor);
	}

}
